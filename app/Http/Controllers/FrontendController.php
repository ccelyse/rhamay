<?php

namespace App\Http\Controllers;

use App\Attractions;
use App\JoinMember;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function Home(){
        return view('welcome');
    }
    public function AboutUs(){
        return view('Aboutus');
    }
    public function BecomeMember(){
        return view('BecomeMember');
    }

    public function JoinMembers(Request $request){
        $all = $request->all();

        $messages = [
            'fileToUpload' => 'The file to upload must be in PDF format',
        ];
        $request->validate([
            'fileToUpload'   => 'mimes:pdf'
        ],$messages);

        $companyname = $request['companyname'];
        $JoinM = new JoinMember();
        $Memberstatus = 'No Status yet';
        $image = $request->file('fileToUpload');

        $JoinM->typeofmembership = $request['typeofmembership'];
        $JoinM->chamber = $request['chamber'];
        $JoinM->companyname = $request['companyname'];
        $JoinM->companycode = $request['companycode'];
        $JoinM->hotelcategory = $request['hotelcategory'];
        $JoinM->numberofrooms = $request['numberofrooms'];
        $JoinM->gender = $request['gender'];
        $JoinM->phonenumber = $request['phonenumber'];
        $JoinM->pobx = $request['pobx'];
        $JoinM->email = $request['email'];
        $JoinM->website = $request['website'];
        $JoinM->businessaddress = $request['businessaddress'];
        $JoinM->buildingname = $request['buildingname'];
        $JoinM->businessarea = $request['businessarea'];
        $JoinM->province = $request['province'];
        $JoinM->district = $request['district'];
        $JoinM->sector = $request['sector'];
        $JoinM->cell = $request['cell'];
        $JoinM->companytype = $request['companytype'];
        $JoinM->ownership = $request['ownership'];
        $JoinM->businessactivity = $request['businessactivity'];
        $JoinM->export = $request['export'];
        $JoinM->numberofemployees = $request['numberofemployees'];
        $JoinM->numberofemployeesapart = $request['numberofemployeesapart'];
        $JoinM->Memberstatus = $Memberstatus;
        $JoinM->user_id = '1';
        $JoinM->documentname = $image->getClientOriginalName();

        $imagename = $image->getClientOriginalName();

        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/rdbcertificate');

        $image->move($destinationPath, $imagename);

        $JoinM->save();

        return view('Thankyou')->with(['companyname'=>$companyname]);
    }
    public function BusinessLicensing(){
        return view('BusinessLicensing');
    }
    public function News(){
        $newpost = Post::orderBy('created_at','desc')->get();
        return view('News')->with(['newpost'=>$newpost]);
    }
    public function NewsReadMore(Request $request){
        $id = $request['id'];
        $newpost = Post::where('id',$id)->get();
        return view('NewsReadMore')->with(['newpost'=>$newpost]);
    }
    public function Publications(){
        return view('Publications');
    }
    public function ContactUs(){
        return view('ContactUs');
    }
    public function Thankyou(){
        return view('Thankyou');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function MemberDirectory(){
        $listcat = JoinMember::where('approval','Approved')->get();
        return view('MemberDirectory')->with(['listcat'=>$listcat]);
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
//    public function AttractionAPI(){
//        return view('AttractionAPI');
//    }

    public function AttractionAPI(){
        $listattractions = Attractions::select('id','attraction_province','attraction_name','attraction_image')->get();
        return response()->json($listattractions);
    }
    public function MoreAttractionAPI(Request $request){
        $id = $request['id'];
        $listattractions = Attractions::where('id',$id)->get();
        return response()->json($listattractions);
    }

    public function AttractionsMore(Request $request){
        $all = $request->all();
        $attraction_id = $request['id'];
        $listmore = Attractions::where('id',$attraction_id)->get();
        $listcountries = Countries::all();
        return view('AttractionsMore')->with(['listmore'=>$listmore,'listcountries'=>$listcountries]);

    }

}
