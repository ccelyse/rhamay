@extends('layouts.master')

@section('title', 'RHA')

@section('content')
    <style>
        .spb-asset-content p {
            color: #000 !important;
        }
        .title-wrap .spb-heading>span {
            display: inline-block;
            text-transform: uppercase;
        }
    </style>
    @include('layouts.topmenu')

<div id="sf-mobile-slideout-backdrop"></div>
<div id="main-container" class="clearfix">
    <div class="fancy-heading-wrap  fancy-style">
        <div class="page-heading fancy-heading clearfix light-style fancy-image  page-heading-breadcrumbs" style="background-image: url('images/About Us-03.jpg');" data-height="475" data-img-width="2000" data-img-height="800">
            <span class="media-overlay" style=""></span>
            <div class="heading-text container" data-textalign="left">
                <h1 class="entry-title">About RHA</h1>
            </div>
        </div>
    </div>
    <div class="inner-container-wrap">
        <div class="inner-page-wrap has-no-sidebar no-bottom-spacing no-top-spacing clearfix">
            <div class="clearfix">
                <div class="page-content hfeed clearfix">
                    <div class="clearfix post-13116 page type-page status-publish hentry" id="13116">
                        <section class="container ">
                            <div class="row">
                                <div class="blank_spacer col-sm-12  " style="height:60px;"></div>
                            </div>
                        </section>
                        <section class="container ">
                            <div class="row">
                                <div class="spb_content_element col-sm-6 spb_text_column">
                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                        <div class="title-wrap">
                                            <h3 class="spb-heading spb-text-heading"><span>A brief history about RHA</span></h3>
                                        </div>
                                        <p>The Rwanda Hospitality Association (RHA) is one of the five Associations that make up the Chamber of Tourism which is one of the ten professional Chambers that currently exist under the umbrella of the Rwanda Private Sector Federation (PSF). </p>
                                        <p>In 2001, the Rwanda Chamber of Commerce (PSF now) established a framework under which private players in same sectors of operation would align with the Government vision 2020.</p>
                                        <p>A few hoteliers by then across the country convened and formed the Rwanda Hotels, Bars and Restaurants Association (RHRA) which started with a membership composition of 10-20 members by 2002.
                                        </p>
                                        <p>In 2012, RHA was published in the national gazette  which under PSF and through the Tourism Chamber grants it with the autonomy to advocate and represent members as the overall national trade association.
                                        </p>
                                        <div class="title-wrap">
                                            <h3 class="spb-heading spb-text-heading"><span>RHA Composition</span></h3>
                                        </div>
                                        <ul class="sf-list ">
                                            <li><i class="sf-icon-right-chevron"></i><span>Hotels</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Resorts</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Apartments</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Restaurants</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Game lodges</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Guest houses</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Night clubs
</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Eco lodges
</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Coffee shops
</span></li>
                                            <li><i class="sf-icon-right-chevron"></i><span>Bars</span></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="spb_content_element col-sm-6 spb_text_column">
                                    <div class="spb-asset-content" style="padding-top:0%;padding-bottom:0%;padding-left:0%;padding-right:0%;">
                                        <div class="title-wrap">
                                            <h3 class="spb-heading spb-text-heading"><span>Mission</span></h3>
                                        </div>
                                        <p>The mission of Rwanda Hospitality Association (RHA) is to represent the common interest of members in the Hospitality Industry through lobby for industry friendly legislation, promotion of quality products and services, offering value added member services, and to be the resource for industry information and education to our members, their employees, government, media, and all other interested parties.</p>
                                        <div class="title-wrap">
                                            <h3 class="spb-heading spb-text-heading"><span>Vision</span></h3>
                                        </div>
                                        <p>To position as the advocate of all operators in Hospitality related businesses by being key resource for reliable information and capacity building for the Hospitality & Tourism Industry in Rwanda.</p>

                                    </div>
                                </div>
                            </div>
                        </section>
                        <section data-header-style="" class="row fw-row  dynamic-header-change">
                            <div class="spb-row-container spb-row-content-width spb_parallax_asset sf-parallax parallax-content-height parallax-scroll spb_content_element bg-type-cover col-sm-12  col-natural">
                                <div class="spb_content_element" style="background-image: url(images/rad.jpg);background-position: center;background-size: cover;">
                                    <section class="container ">
                                        <div class="row">
                                            <div class="blank_spacer col-sm-12" style="height:60px;"></div>
                                        </div>
                                    </section>
                                    <section class="container ">
                                        <div class="row">
                                            <div class="team_list carousel-asset spb_content_element col-sm-12">
                                                <div class="spb-asset-content">
                                                    <div class="title-wrap clearfix">
                                                        <h3 class="spb-heading" style="color: #fff !important;"><span>Meet Our Executive Committee </span></h3>
                                                        <div class="carousel-arrows"><a href="index.html#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.html#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>
                                                    </div>
                                                    <div class="team-carousel carousel-wrap">
                                                        <div id="carousel-1" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="5" data-auto="false">
                                                            <div itemscope data-id="id-0" class="clearfix team-member carousel-item">
                                                                <div class="team-member-item-wrap">
                                                                    <figure class="animated-overlay">
                                                                        <a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="" width="270" height="270" alt="NSENGIYUNVA BARAKABUYE" />
                                                                        <figcaption class="team-standard-alt">
                                                                            <div class="thumb-info thumb-info-alt">
                                                                                <i>
                                                                                    <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                        <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                        <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                </i>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div class="team-member-details-wrap">
                                                                        <h4 class="team-member-name"><a href="" class="" data-id="212">NSENGIYUNVA BARAKABUYE</a></h4>
                                                                        <h5 class="team-member-position">Chairman</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div itemscope data-id="id-1" class="clearfix team-member carousel-item">
                                                                <div class="team-member-item-wrap">
                                                                    <figure class="animated-overlay">
                                                                        <a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Robert TWEBAZE" />
                                                                        <figcaption class="team-standard-alt">
                                                                            <div class="thumb-info thumb-info-alt">
                                                                                <i>
                                                                                    <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                        <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                        <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                </i>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div class="team-member-details-wrap">
                                                                        <h4 class="team-member-name"><a href="" class="" data-id="213">Robert TWEBAZE</a></h4>
                                                                        <h5 class="team-member-position">1St Vice Chairman</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div itemscope data-id="id-2" class="clearfix team-member carousel-item">
                                                                <div class="team-member-item-wrap">
                                                                    <figure class="animated-overlay">
                                                                        <a class="team-member-link " href="" data-id="214"></a><img itemprop="image" src="" width="270" height="270" alt="Jeanette RUGERA" />
                                                                        <figcaption class="team-standard-alt">
                                                                            <div class="thumb-info thumb-info-alt">
                                                                                <i>
                                                                                    <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                        <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                        <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                </i>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div class="team-member-details-wrap">
                                                                        <h4 class="team-member-name"><a href="" class="" data-id="214">Jeanette RUGERA </a></h4>
                                                                        <h5 class="team-member-position">2nd Vice Chairman</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div itemscope data-id="id-3" class="clearfix team-member carousel-item">
                                                                <div class="team-member-item-wrap">
                                                                    <figure class="animated-overlay">
                                                                        <a class="team-member-link " href="" data-id="215"></a><img itemprop="image" src="" width="270" height="270" alt="Nicole ANSONI " />
                                                                        <figcaption class="team-standard-alt">
                                                                            <div class="thumb-info thumb-info-alt">
                                                                                <i>
                                                                                    <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                        <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                        <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                </i>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div class="team-member-details-wrap">
                                                                        <h4 class="team-member-name"><a href="" class="" data-id="215">Nicole ANSONI </a></h4>
                                                                        <h5 class="team-member-position">Secretary</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div itemscope data-id="id-4" class="clearfix team-member carousel-item">
                                                                <div class="team-member-item-wrap">
                                                                    <figure class="animated-overlay">
                                                                        <a class="team-member-link " href="" data-id="222"></a><img itemprop="image" src="" width="270" height="270" alt="RUKUNDO Aimable" />
                                                                        <figcaption class="team-standard-alt">
                                                                            <div class="thumb-info thumb-info-alt">
                                                                                <i>
                                                                                    <svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                                                  <path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />
                                                                                        <path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />
                                                                                        <path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />
                                                               </svg>
                                                                                </i>
                                                                            </div>
                                                                        </figcaption>
                                                                    </figure>
                                                                    <div class="team-member-details-wrap">
                                                                        <h4 class="team-member-name"><a href="" class="" data-id="222">RUKUNDO Aimable </a></h4>
                                                                        <h5 class="team-member-position">Treasurer</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--<div class="team_list carousel-asset spb_content_element col-sm-12">-->
                                            <!--<div class="spb-asset-content">-->
                                            <!--<div class="title-wrap clearfix">-->
                                            <!--<h3 class="spb-heading" style="color: #fff !important;"><span>Meet Our Zone Leaders </span></h3>-->
                                            <!--<div class="carousel-arrows"><a href="index.html#" class="carousel-prev"><i class="sf-icon-left-chevron"></i></a><a href="index.html#" class="carousel-next"><i class="sf-icon-right-chevron"></i></a></div>-->
                                            <!--</div>-->
                                            <!--<div class="team-carousel carousel-wrap">-->
                                            <!--<div id="carousel-2" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="5" data-auto="false">-->
                                            <!--<div itemscope data-id="id-0" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="212"></a><img itemprop="image" src="" width="270" height="270" alt="Jeanine NDEZE" />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="212">Jeanine NDEZE</a></h4>-->
                                            <!--<h5 class="team-member-position">Zone Leader RUBAVU</h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Jacob TUMWINE" />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Jacob TUMWINE </a></h4>-->
                                            <!--<h5 class="team-member-position">zone Leader KARONGI</h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->

                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Jean de Dieu NKURUNZIZA" />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Jean de Dieu NKURUNZIZA, </a></h4>-->
                                            <!--<h5 class="team-member-position">1st Vice Zone Leader - EAST</h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->


                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Jean Paul NSHIMIYUMUREMYI " />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Jean Paul NSHIMIYUMUREMYI </a></h4>-->
                                            <!--<h5 class="team-member-position">Zone Leader South NYAMAGABE & HUYE </h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->



                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Jacob TUMWINE" />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Jacob TUMWINE </a></h4>-->
                                            <!--<h5 class="team-member-position">zone Leader KARONGI</h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->

                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--<div class="team-carousel carousel-wrap">-->
                                            <!--<div id="carousel-4" class="team-members carousel-items display-type-standard-alt clearfix" data-columns="5" data-auto="false">-->


                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Ferdinand HAGABIMANA " />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Ferdinand HAGABIMANA  </a></h4>-->
                                            <!--<h5 class="team-member-position">Zone Leader MUSANZE </h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->

                                            <!--<div itemscope data-id="id-1" class="clearfix team-member carousel-item">-->
                                            <!--<div class="team-member-item-wrap">-->
                                            <!--<figure class="animated-overlay">-->
                                            <!--<a class="team-member-link " href="" data-id="213"></a><img itemprop="image" src="" width="270" height="270" alt="Eugene RUTAGARAMA" />-->
                                            <!--<figcaption class="team-standard-alt">-->
                                            <!--<div class="thumb-info thumb-info-alt">-->
                                            <!--<i>-->
                                            <!--<svg version="1.1" class="sf-hover-svg svg-team" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">-->
                                            <!--<path class="delay-1" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,12h13" />-->
                                            <!--<path fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,5h22" />-->
                                            <!--<path class="delay-2" fill="none" stroke="#444444" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M1,19h22" />-->
                                            <!--</svg>-->
                                            <!--</i>-->
                                            <!--</div>-->
                                            <!--</figcaption>-->
                                            <!--</figure>-->
                                            <!--<div class="team-member-details-wrap">-->
                                            <!--<h4 class="team-member-name"><a href="" class="" data-id="213">Eugene RUTAGARAMA </a></h4>-->
                                            <!--<h5 class="team-member-position">Zone Leader RUSIZI & NYAMASHEKE </h5>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->


                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->
                                            <!--</div>-->

                                        </div>
                                    </section>

                                </div>
                            </div>
                        </section>
                        <div class="link-pages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sf-full-header-search-backdrop"></div>
</div>
@include('layouts.footer')
@endsection