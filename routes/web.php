<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Home',['as'=>'welcome','uses'=>'FrontendController@Home']);
Route::get('/AboutUs',['as'=>'Aboutus','uses'=>'FrontendController@AboutUs']);
Route::get('/BecomeMember', ['as'=>'BecomeMember','uses'=>'FrontendController@BecomeMember']);
Route::get('/MemberDirectory', ['as'=>'MemberDirectory','uses'=>'FrontendController@MemberDirectory']);
Route::post('/JoinMembers', ['as'=>'BecomeMember_','uses'=>'FrontendController@JoinMembers']);
Route::get('/BusinessLicensing', ['as'=>'BusinessLicensing','uses'=>'FrontendController@BusinessLicensing']);
Route::get('/News', ['as'=>'News','uses'=>'FrontendController@News']);
Route::get('/NewsReadMore', ['as'=>'NewsReadMore','uses'=>'FrontendController@NewsReadMore']);
Route::get('/Publications', ['as'=>'Publications','uses'=>'FrontendController@Publications']);
Route::get('/JoinsUs', ['as'=>'JoinsUs','uses'=>'FrontendController@BecomeMember']);
Route::get('/Thankyou', ['as'=>'Thankyou','uses'=>'FrontendController@Thankyou']);
Route::get('/VisitRwanda', ['as' => 'VisitRwanda', 'uses' => 'BackendController@VisitRwanda']);
Route::get('/AttractionsMore', ['as' => 'AttractionsMore', 'uses' => 'BackendController@AttractionsMore']);
Route::post('/Hireaguide', ['as' => 'Hireaguide', 'uses' => 'BackendController@Hireaguide']);

Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/Webmail', ['as'=>'Webmail','uses'=>'FrontendController@Webmail']);

//Route::get('/AttractionAPI', ['as' => 'AttractionAPI', 'uses' => 'FrontendController@AttractionAPI']);
Route::get('/AttractionAPI', ['as'=>'AttractionAPI','uses'=>'FrontendController@AttractionAPI']);
Route::get('/Test', ['as'=>'Test','uses'=>'FrontendController@AttractionAPI']);



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'disablepreventback'],function(){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::get('/ListOfMembers', ['as' => 'backend.ListOfMembers', 'uses' => 'BackendController@ListOfMembers']);
        Route::get('/FilterMembers', ['as' => 'backend.FilterMembers', 'uses' => 'BackendController@FilterMembers']);
        Route::post('/FilterMembers_', ['as' => 'backend.FilterMembers_', 'uses' => 'BackendController@FilterMembers_']);
        Route::get('/ApproveMember', ['as' => 'backend.ApproveMember', 'uses' => 'BackendController@ApproveMember']);
        Route::get('/EditJoinMember', ['as' => 'backend.EditJoinMember', 'uses' => 'BackendController@EditJoinMember']);
        Route::post('/EditJoinMember_', ['as' => 'backend.EditJoinMember_', 'uses' => 'BackendController@EditJoinMember_']);

        Route::get('/AddAttractions', ['as' => 'backend.AddAttractions', 'uses' => 'BackendController@AddAttractions']);
        Route::get('/AddNews', ['as' => 'backend.AddNews', 'uses' => 'BackendController@AddNews']);
        Route::post('/AddNews_', ['as' => 'backend.AddNews_', 'uses' => 'BackendController@AddNews_']);
        Route::get('/NewsList', ['as' => 'backend.NewsList', 'uses' => 'BackendController@NewsList']);
        Route::get('/EditNews', ['as' => 'backend.EditNews', 'uses' => 'BackendController@EditNews']);
        Route::post('/EditNews_', ['as' => 'backend.EditNews_', 'uses' => 'BackendController@EditNews_']);
        Route::get('/DeleteNews', ['as' => 'backend.DeleteNews', 'uses' => 'BackendController@DeleteNews']);
        Route::get('/GuidesList', ['as' => 'backend.GuidesRequest', 'uses' => 'BackendController@GuidesList']);
        Route::get('/BankSlip', ['as' => 'backend.BankSlip', 'uses' => 'BackendController@BankSlip']);
        Route::get('/AddBankSlip', ['as' => 'backend.AddBankSlip', 'uses' => 'BackendController@AddBankSlip']);
        Route::post('/BankSlip_', ['as' => 'backend.BankSlip_', 'uses' => 'BackendController@BankSlip_']);
        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
        Route::get('/SendSMS', ['as' => 'backend.SendSMS', 'uses' => 'BackendController@SendSMS']);
        Route::post('/SendSMS_', ['as' => 'backend.SendSMS_', 'uses' => 'BackendController@SendSMS_']);
        Route::get('/SendEmail', ['as' => 'backend.SendEmail', 'uses' => 'BackendController@SendEmail']);
        Route::post('/SendEmail_', ['as' => 'backend.SendEmail_', 'uses' => 'BackendController@SendEmail_']);

        Route::get('/CompanyCategory', ['as' => 'backend.CompanyCategory', 'uses' => 'BackendController@CompanyCategory']);
        Route::post('/CompanyCategory_', ['as' => 'backend.CompanyCategory_', 'uses' => 'BackendController@CompanyCategory_']);
        Route::get('/EditCompanyCategory', ['as' => 'backend.EditCompanyCategory', 'uses' => 'BackendController@EditCompanyCategory']);
        Route::post('/EditCompanyCategory_', ['as' => 'backend.EditCompanyCategory_', 'uses' => 'BackendController@EditCompanyCategory_']);
        Route::get('/DeleteCompanycategory', ['as' => 'backend.DeleteCompanycategory', 'uses' => 'BackendController@DeleteCompanycategory']);


    });
});





