<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\Attractions;
use App\CompanyCategory;
use App\Countries;
use App\Hireaguide;
use App\JoinMember;
use App\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{
    public function SignIn_(){

    }
    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        /*$now = Carbon::now();
        $nowdate = Carbon::now();
        $nowyear = $now->addYear(2);


        $day = strtotime($now->toDateString());

        $renewdate = JoinMember::whereBetween('expiration_date',[$nowdate,$nowyear])->get();

        foreach ($renewdate as $datas){
            $date_renew = $datas->expiration_date;
            $dateonly = strtotime($date_renew);
//            echo "<p>$dateonly</p>";
            $nowexp = time();
            $datediff = $dateonly - $nowexp;
            $daysexpiration = round($datediff / (60 * 60 * 24));

            if ($daysexpiration >= 30){

            }
        }*/


        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         *  application member
         */

        $appmember = JoinMember::select(DB::raw('count(id) as appmember'))->value('appmember');

        $approvedmember = JoinMember::select(DB::raw('count(id) as approvedmember'))
            ->where('approval','Approved')
            ->value('approvedmember');

        $deniedmember = JoinMember::select(DB::raw('count(id) as deniedmember'))
            ->where('approval','Denied')
            ->value('deniedmember');

        $guiderequest = Hireaguide::select(DB::raw('count(id) as guiderequest'))->value('guiderequest');

        /**
         *
         * MEmber application  (JANUARY)
         */

        $janmember = JoinMember::select(DB::raw('count(id) as janmember'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('janmember');

        /**
         *
         * MEmber application  (Feb)
         */

        $febmember = JoinMember::select(DB::raw('count(id) as febmember'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('febmember');

        /**
         *
         * MEmber application  (mar)
         */

        $marmember = JoinMember::select(DB::raw('count(id) as marmember'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('marmember');
        /**
         *
         * MEmber application  (apr)
         */

        $aprmember = JoinMember::select(DB::raw('count(id) as aprmember'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('aprmember');

        /**
         *
         * MEmber application  (may)
         */

        $maymember = JoinMember::select(DB::raw('count(id) as maymember'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('maymember');

        /**
         *
         * MEmber application  (jun)
         */

        $junmember = JoinMember::select(DB::raw('count(id) as junmember'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('junmember');

        /**
         *
         * MEmber application  (jul)
         */

        $julmember = JoinMember::select(DB::raw('count(id) as julmember'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('julmember');

        /**
         *
         * MEmber application  (aug)
         */

        $augmember = JoinMember::select(DB::raw('count(id) as augmember'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('augmember');

        /**
         *
         * MEmber application  (sep)
         */

        $sepmember = JoinMember::select(DB::raw('count(id) as sepmember'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('sepmember');


        /**
         *
         * MEmber application  (oct)
         */


        $octmember = JoinMember::select(DB::raw('count(id) as octmember'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('octmember');

        /**
         *
         * MEmber application  (nov)
         */


        $novmember = JoinMember::select(DB::raw('count(id) as novmember'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('novmember');

        /**
         *
         * MEmber application  (dec)
         */

        $decmember = JoinMember::select(DB::raw('count(id) as decmember'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('decmember');

        return view('backend.Dashboard')->with(['janmember'=>$janmember,'febmember'=>$febmember,
            'marmember'=>$marmember,'aprmember'=>$aprmember,'maymember'=>$maymember,'junmember'=>$junmember,'julmember'=>$julmember,
            'augmember'=>$augmember,'sepmember'=>$sepmember,'octmember'=>$octmember,'novmember'=>$novmember,'decmember'=>$decmember,
            'appmember'=>$appmember,'approvedmember'=>$approvedmember,'deniedmember'=>$deniedmember,'guiderequest'=>$guiderequest]);
    }
    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function ListOfMembers(){
        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.ListOfMembers')->with(['listmembers'=>$listmembers]);

    }
    public function FilterMembers(){

        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.FilterMembers')->with(['listmembers'=>$listmembers]);
    }
    public function FilterMembers_(Request $request){
        $all = $request->all();
        $district = $request['district'];
        $filter = JoinMember::where('district', 'LIKE', '%'.$district.'%')->get();
        return view('backend.FilterMembers')->with(['filter'=>$filter]);

    }

    public function ApproveMember(Request $request){

        $id = $request['MemberId'];
        $status = $request['Status'];
        $currentMonth = Carbon::now()->format('m');
        $code = 'RHA'. mt_rand(10, 99). $currentMonth;
        $date_now = Carbon::now();
        $dateafteryear = Carbon::now()->addYear();


        switch ($status) {
            case "Approved":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Approved';
                $update_member->Memberstatus ='Approved';
                $update_member->approved_date =$date_now;
                $update_member->expiration_date =$dateafteryear;
                $update_member->identification =$code;
                $update_member->save();
                break;

            case "Denied":
                $update_member = JoinMember::find($id);
                $update_member->approval ='Denied';
                $update_member->Memberstatus ='Denied';
                $update_member->identification ='';
                $update_member->approved_date =NULL;
                $update_member->expiration_date =NULL;
                $update_member->save();
                break;

        }
        return back()->with('success','You have successfully updated members list');
    }
    public function AddAttractions(){
        return view('backend.AddAttractions');
    }
    public function EditJoinMember(Request $request){
        $id = $request['id'];

        $edit = JoinMember::where('id',$id)->get();

        return view('backend.EditJoinMember')->with(['edit'=>$edit]);
    }
    public function EditJoinMember_(Request $request){
        $id  = $request['id'];
        $all = $request->all();
        $Memberstatus = 'No Status yet';
        $image = $request->file('fileToUpload');
        $user_id = \Auth::user()->id;


        if ($request->hasFile('fileToUpload')) {
            $EditJoinM = JoinMember::find($id);
            $EditJoinM->typeofmembership = $request['typeofmembership'];
            $EditJoinM->chamber = $request['chamber'];
            $EditJoinM->companyname = $request['companyname'];
            $EditJoinM->companycode = $request['companycode'];
            $EditJoinM->hotelcategory = $request['hotelcategory'];
            $EditJoinM->numberofrooms = $request['numberofrooms'];
            $EditJoinM->gender = $request['gender'];
            $EditJoinM->phonenumber = $request['phonenumber'];
            $EditJoinM->pobx = $request['pobx'];
            $EditJoinM->email = $request['email'];
            $EditJoinM->website = $request['website'];
            $EditJoinM->businessaddress = $request['businessaddress'];
            $EditJoinM->buildingname = $request['buildingname'];
            $EditJoinM->businessarea = $request['businessarea'];
            $EditJoinM->province = $request['province'];
            $EditJoinM->district = $request['district'];
            $EditJoinM->sector = $request['sector'];
            $EditJoinM->cell = $request['cell'];
            $EditJoinM->companytype = $request['companytype'];
            $EditJoinM->ownership = $request['ownership'];
            $EditJoinM->businessactivity = $request['businessactivity'];
            $EditJoinM->export = $request['export'];
            $EditJoinM->numberofemployees = $request['numberofemployees'];
            $EditJoinM->numberofemployeesapart = $request['numberofemployeesapart'];
            $EditJoinM->user_id = $user_id;
            $EditJoinM->documentname = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/rdbcertificate');
            $image->move($destinationPath, $imagename);
            $EditJoinM->save();
            return redirect()->route('backend.ListOfMembers')->with('update','You have successfully updated Member Information');
        }else{
            $EditJoinM = JoinMember::find($id);
            $EditJoinM->typeofmembership = $request['typeofmembership'];
            $EditJoinM->chamber = $request['chamber'];
            $EditJoinM->companyname = $request['companyname'];
            $EditJoinM->companycode = $request['companycode'];
            $EditJoinM->hotelcategory = $request['hotelcategory'];
            $EditJoinM->numberofrooms = $request['numberofrooms'];
            $EditJoinM->gender = $request['gender'];
            $EditJoinM->phonenumber = $request['phonenumber'];
            $EditJoinM->pobx = $request['pobx'];
            $EditJoinM->email = $request['email'];
            $EditJoinM->website = $request['website'];
            $EditJoinM->businessaddress = $request['businessaddress'];
            $EditJoinM->buildingname = $request['buildingname'];
            $EditJoinM->businessarea = $request['businessarea'];
            $EditJoinM->province = $request['province'];
            $EditJoinM->district = $request['district'];
            $EditJoinM->sector = $request['sector'];
            $EditJoinM->cell = $request['cell'];
            $EditJoinM->companytype = $request['companytype'];
            $EditJoinM->ownership = $request['ownership'];
            $EditJoinM->businessactivity = $request['businessactivity'];
            $EditJoinM->export = $request['export'];
            $EditJoinM->numberofemployees = $request['numberofemployees'];
            $EditJoinM->numberofemployeesapart = $request['numberofemployeesapart'];
            $EditJoinM->user_id = $user_id;
            $EditJoinM->save();
            return redirect()->route('backend.ListOfMembers')->with('update','You have successfully updated Member Information');
        }
    }
    public function AddAttractions_(Request $request){
        $all = $request->all();
//        dd($all);
//        $request->validate([
//            'fileToUpload'   => 'mimes:pdf'
//        ]);

        $saveattractions = new Attractions();

        $image = $request->file('fileToUpload');

        $saveattractions->attraction_province = $request['attraction_province'];
        $saveattractions->attraction_name = $request['attraction_name'];
        $saveattractions->attraction_indetails = $request['attraction_indetails'];

        $saveattractions->attraction_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/attractions');
        $image->move($destinationPath, $imagename);

        $saveattractions->save();

        return back()->with('success','you have created a new attraction');
    }
    public function AddNews(){
        return view('backend.AddNews');
    }
    public function AddNews_(Request $request){
        $all = $request->all();
        $addnews = new Post();
        $addnews->news_title = $request['news_title'];
        $addnews->news_details = $request['news_details'];
        $addnews->newsdescription = $request['newsdescription'];
        $addnews->save();
        return back()->with('success','you have created a new post');
    }
    public function NewsList(){
        $listnews = Post::all();
        return view('backend.NewsList')->with(['listnews'=>$listnews]);
    }
    public function EditNews(Request $request){
        $id = $request['id'];
        $editnews = Post::where('id',$id)->get();

        return view('backend.EditNews')->with(['editnews'=>$editnews]);
    }
    public function EditNews_(Request $request){
        $id = $request['id'];
        $editnews = Post::find($id);
        $editnews->news_title = $request['news_title'];
        $editnews->news_details = $request['news_details'];
        $editnews->newsdescription = $request['newsdescription'];
        $editnews->save();
        return redirect()->route('backend.NewsList')->with('success','You have successful updated a Post');


    }
    public function DeleteNews(Request $request){
        $id = $request['id'];
        $deletebrand = Post::find($id);
        $deletebrand->delete();
        return back()->with('success','you successfully deleted a post');
    }
    public function VisitRwanda()
    {
        $listattractions = Attractions::all();
        return view('VisitRwanda')->with(['listattractions' => $listattractions]);
    }
    public function AttractionsMore(Request $request){
        $all = $request->all();
        $attraction_id = $request['id'];
        $listmore = Attractions::where('id',$attraction_id)->get();
        $listcountries = Countries::all();
        return view('AttractionsMore')->with(['listmore'=>$listmore,'listcountries'=>$listcountries]);

    }
    public function Hireaguide(Request $request){

        $hireaguide = new Hireaguide();
        $hireaguide->attractionid = $request['attractionid'];
        $hireaguide->name = $request['name'];
        $hireaguide->email = $request['email'];
        $hireaguide->contactnumber = $request['contactnumber'];
        $hireaguide->country = $request['country'];
        $hireaguide->totaldults = $request['totaldults'];
        $hireaguide->totalchildren = $request['totalchildren'];
        $hireaguide->message = $request['message'];

        $hireaguide->save();

        return back()->with('success','Thank you for requesting a guide! we will get back to you with 24 hours');

    }
    public function GuidesList(){
        $listguides = Hireaguide::all();
        return view('backend.GuidesRequest')->with(['listguides'=>$listguides]);
    }
    public function BankSlip(Request $request){
        $id = $request['id'];
        return view('backend.BankSlip')->with(['id'=>$id]);
    }
    public function AddBankSlip(Request $request){
        $id  = $request['id'];
        return view('backend.AddBankSlip')->with(['id'=>$id]);
    }
    public function BankSlip_(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $addbankslip = JoinMember::find($id);
        $image = $request->file('fileToUpload');
        $addbankslip->bankslip = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/bankslip');
        $image->move($destinationPath, $imagename);
        $addbankslip->save();
        return redirect()->route('backend.ListOfMembers')->with('success','you have successfully added bank slip');
    }
    public function SendSMS(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendSMS')->with(['listnumber'=>$listnumber]);
    }
    public function SendSMS_(Request $request){
//        $all = $request->all();
//        dd($all);


//        $username   = "rha";
//        $apiKey     = "55fa2436c4624ed856c4910111fde3ecd425a6bc738ed2fc20d6c85c438258b8";
        $message = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $pluscode = '+25';
        $recipients = $pluscode . $phonenumber;

        $username = 'sandbox'; // use 'sandbox' for development in the test environment
        $apiKey 	= 'fa1c39e0943a7802136e51e9b04acc43367003b517b09e6954463a688c27315f'; // use your sandbox app API key for development in the test environment
        $AT = new AfricasTalking($username, $apiKey,"sandbox");
        $SMS = $AT->sms();
        $options = array(
            "to" => '+250782384772',
            "message" => "$message",
        );

        $SMS->send($options);
        return redirect()->route('backend.SendSMS')->with('success','you have successfully sent your message');
    }
    public function SendEmail(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendEmail')->with(['listnumber'=>$listnumber]);
    }
    public function SendEmail_(Request $request){
        $all = $request->all();
        dd($all);
    }
    public function CompanyCategory(){
        $listcomp = CompanyCategory::all();
        return view('backend.CompanyCategory')->with(['listcomp'=>$listcomp]);
    }
    public function CompanyCategory_(Request $request){
        $all = $request->all();

        $addcompanycategory = new CompanyCategory();
        $addcompanycategory->company_category = $request['companycategory'];
        $addcompanycategory->save();

        return back()->with('success','you successfully added a new company category');

    }
    public function EditCompanyCategory(Request $request){
        $id = $request['id'];
        $editcomp = CompanyCategory::where('id',$id)->get();
        return view('backend.EditCompanyCategory')->with(['editcomp'=>$editcomp]);
    }
    public function EditCompanyCategory_(Request $request){
        $id  = $request['id'];
        $updatecomp = CompanyCategory::find($id);
        $updatecomp->company_category = $request['companycategory'];
        $updatecomp->save();

        return redirect()->route('backend.CompanyCategory')->with('success','You have successful company category');
    }
    public function DeleteCompanycategory(Request $request){
        $id = $request['id'];
        $deletecomp = CompanyCategory::find($id);
        $deletecomp->delete();
        return back();
    }
}
