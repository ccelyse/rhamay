<?php

namespace App\Http\Controllers;

use App\JoinMember;
use Illuminate\Http\Request;
use App\Http\Resources\ListMemberResource as ListMemberResource;

class ListMembers extends Controller
{
    public function index()
    {
        return JoinMember::all();
    }

    public function show ($id)
    {
//        return new ListMemberResource(JoinMember::all());
        return new ListMemberResource(JoinMember::find($id));
    }
}
